;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((indent-tabs-mode . t)
	 (fill-column . 70)))
 (python-mode . ((indent-tabs-mode . nil)
		 (fill-column . 78))))
