Format: 1.0
Source: pari-extra
Binary: pari-extra
Architecture: all
Version: 3-2~dummy1
Maintainer: Bill Allombert <ballombe@debian.org>
Standards-Version: 3.9.2.0
Build-Depends: debhelper (>= 5), package-does-not-exist
Package-List:
 pari-extra deb math optional arch=all
Checksums-Sha1:
 ff281e103ab11681324b0c694dd3514d78436c51 121 pari-extra_3.orig.tar.gz
 335afa3b9e4b671a67d00e699be080df44fe08fa 2486 pari-extra_3-2~dummy1.diff.gz
Checksums-Sha256:
 ac1ef39f9da80b582d1c0b2adfb09b041e3860ed20ddcf57a0e922e3305239df 121 pari-extra_3.orig.tar.gz
 2365210b7a21a28659747188464ae1b5869accc714212f6d5d0c6632899c2ca0 2486 pari-extra_3-2~dummy1.diff.gz
Files:
 76bcf03be979d3331f9051aa88439b8b 121 pari-extra_3.orig.tar.gz
 5a4ffde2059a1c4c7280bf63ca99991d 2486 pari-extra_3-2~dummy1.diff.gz
