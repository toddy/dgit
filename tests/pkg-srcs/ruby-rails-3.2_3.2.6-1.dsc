Format: 3.0 (quilt)
Source: ruby-rails-3.2
Binary: ruby-rails-3.2, rails3
Architecture: all
Version: 3.2.6-1
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Antonio Terceiro <terceiro@debian.org>
Homepage: http://www.rubyonrails.org
Standards-Version: 3.9.3
Vcs-Browser: http://git.debian.org/?p=pkg-ruby-extras/ruby-rails.git;a=summary
Vcs-Git: git://git.debian.org/pkg-ruby-extras/ruby-rails-3.2.git
Build-Depends: debhelper (>= 7.0.50~), gem2deb (>= 0.3.0~)
Package-List:
 rails3 deb ruby optional arch=all
 ruby-rails-3.2 deb ruby optional arch=all
Checksums-Sha1:
 f36c3866b22de8ff6875fdbbfbcfb8d18e1f5a89 953 ruby-rails-3.2_3.2.6.orig.tar.gz
 5a6ca14c46eb4b9297f024675d43002751d560e7 2252 ruby-rails-3.2_3.2.6-1.debian.tar.xz
Checksums-Sha256:
 207cfb1ef70aa9458c776deeda8e38ac977cbc852209828793b27d55bebc7bea 953 ruby-rails-3.2_3.2.6.orig.tar.gz
 ab65b0fe41fff9abb87b538b5d526c2b47af27f784cf4cf2e8c01e399cde7b00 2252 ruby-rails-3.2_3.2.6-1.debian.tar.xz
Files:
 05a3954762c2a2101a10dd2efddf7000 953 ruby-rails-3.2_3.2.6.orig.tar.gz
 e1c519bb58a39d01f4dc6828985057fe 2252 ruby-rails-3.2_3.2.6-1.debian.tar.xz
Ruby-Versions: all
